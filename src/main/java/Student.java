import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Student {
    public String Name;


    public void showByGroup() {

        try {
            String url = "jdbc:postgresql://localhost:5432/postgres";
            Connection conn = DriverManager.getConnection(url,"postgres","9118146e");
            Statement stmt = conn.createStatement();
            ResultSet rs;

            rs = stmt.executeQuery("SELECT  Name FROM Student ORDER BY g_id");
            while ( rs.next() ) {
                String Name = rs.getString("name");
                System.out.println(Name);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "Name='" + Name + '\'' +
                '}';
    }
}
